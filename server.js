console.log("aqui funcionando con Nodemon");

var movimientosJSON = require('./movimientosv2.json')
var express=require('express')
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
var requestJson = require('request-json')

var usuariosJSON = require('./usuariosv2.json')

var app = express()
app.use(bodyparser.json())

//versiòn 3 de la API conectada a MLab

var urlMlabRaiz ="https://api.mlab.com/api/1/databases/techumx21/collections"
var apiKey="apiKey=iNMnSYfcjgHqeQNv__ZP694d9VWQkk_k"
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey)

app.listen(3000)
console.log("Escuchando en el puerto 3000")

app.get('/v5',function(req,res){
  clienteMlab.get('',function(err,resM,body){
    var coleccionesUsuario=[]
    if(!err){
      for (var i = 0; i < body.length; i++) {
        if(body[i]!="system.indexes"){
          coleccionesUsuario.push({"recurso":body[i],"url":"/v5/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else{
      res.send(err)
    }
  })
})

app.get('/v5/loggin/',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios")
  clienteMlab.get('?q={"email":"'+req.headers['email']+'","password":"'+req.headers['password']+'"}&'+apiKey,function(err,resM,body){
    res.send(body)
  })
  console.log(urlMlabRaiz+"/usuarios"+'?q={"email":'+req.headers['email']+'","password":"'+req.headers['password']+'"}&'+apiKey)
})

app.get('/v5/cuentas/:usuario',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas3")
  clienteMlab.get('?q={"usuario":"'+req.params.usuario+'"}&f={"Nucuenta":1, "idcuenta":1, "saldo":1}&'+apiKey,function(err,resM,body){
    res.send(body)
  })
  console.log(urlMlabRaiz+"/cuentas"+'?q={"usuario:"'+req.params.usuario+'"}&f={"Nucuenta":1, "idcuenta":1}&'+apiKey)
})

app.get('/v5/movimientos-o/:idcuenta',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas3")
  clienteMlab.get('?q={"idcuenta":'+req.params.idcuenta+'}&s={"fecha":1}&'+apiKey,function(err,resM,body){
    res.send(body)
  })
  console.log(urlMlabRaiz+"/cuentas"+'?q={"idcuenta:'+req.params.idcuenta+'}&s={"fecha":1}&'+apiKey)
})

app.post('/v5/usuarios', function(req,res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err,resM, body){
    res.send(body)
  })
  console.log(urlMlabRaiz + "/usuarios?" + apiKey +"--"+JSON.stringify(req.body))
})

app.post('/v5/usuariosHead', function(req,res){
  var datos={
    "id": req.headers['id'],
    "email": req.headers['email'],
    "password": req.headers['password'],
    "name": req.headers['name'],
    "estatus": true
  };

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', datos, function(err,resM, body){
    res.send(body)
  })
  console.log(urlMlabRaiz + "/usuarios?" + apiKey +"--"+JSON.stringify(datos))
})

app.get('/v5/usuarios',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey)
  clienteMlab.get('',function(err,resM,body){
    res.send(body)
  })
})

app.post('/v5/movimientos/:idcuenta',function(req,res){
  var bodynuevo=req.body;
  var registrocuenta;
  clienteMlab=requestJson.createClient(urlMlabRaiz + '/cuentas3')
  //Select cuenta con sus movimientos
  clienteMlab.get('?q={"idcuenta":'+req.params.idcuenta+'}&'+apiKey,
    function(err, resM, body){
      registrocuenta=body;
      console.log(body)

      var movactuales=body[0].movimientos;
      bodynuevo.idmov=movactuales.length+1;
      movactuales.push(bodynuevo)

        //Update cuenta con sus movimientos
        var query= '?q={"idcuenta":'+registrocuenta[0].idcuenta+'}&'
        registrocuenta[0].movimientos=movactuales;

          var cambios = '{"$set":'+JSON.stringify(registrocuenta[0])  +'}'
          clienteMlab.put(query+apiKey,JSON.parse(cambios),function(err, resM, body){
          res.send("Alta de movimiento exitosa");

    })
  })
})

/*
req.headers['email']
*/
