var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')

var should = chai.should()
var server = require('../server')
//var server = require('../server.js')

chai.use(chaiHttp) //configurar chai con el modulo https

describe('Tests de conectividad',()=>{
  it('Google funciona', (done)=>{
    chai.request('http://www.google.com.mx').get('/')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      done()
    })
  })
})

describe('Tests de API usuarios',()=>{
  it('Raìz de la API funciona', (done)=>{
    chai.request('http://localhost:3000').get('/v3')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
    })
  })
  it('Raìz de la API devuelve 2 colecciones', (done)=>{
    chai.request('http://localhost:3000').get('/v3')
    .end((err,res)=>{
      //console.log(res)
      console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      done()
    })
  })
  it('Raìz de la API devuelve los objetos correctos', (done)=>{
    chai.request('http://localhost:3000').get('/v3')
    .end((err,res)=>{
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
      }
      done()
    })
  })
})

describe('Tests de API movimientos',()=>{
  it('Raìz de la API movimientos contesta', (done)=>{
    chai.request('http://localhost:3000').get('/v3/movimientos')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
    })
  })
  it('Raìz de la API movimientos idcuenta contesta', (done)=>{
    chai.request('http://localhost:3000').get('/v3/movimientos/MX00001')
    .end((err,res)=>{
      console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')

      res.body[0].should.have.property('idcuenta').which.is.eql("MX00001")
      done()
    })
  })
})
